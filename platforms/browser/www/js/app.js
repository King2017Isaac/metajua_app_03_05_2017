var currentUser = {
		id: null,
		username: null,
		password:null
	},
	app_name = 'Metajua',
	app_version = '0.0.1',
	app = new Framework7( {
	    modalTitle: app_name,
	    material: true,
		animatePages: true
	} ),
	main_view = app.addView( '.view-main', {
	    domCache: true,
		dynamicNavbar: true
	} ),
	pagination = {
		per_page: 15,
		page: 1
	},
	$$ = Dom7,
	db = window.openDatabase( 'metajua_app', app_version, 'Metajua App', 50000 );
	api_base = 'http://192.168.10.101:85/',
	current_module_id = 0,
	current_form_id = 0,
	initialize_form_page = false;

if ( window.localStorage.getItem( 'currentUser' )!= null )
	currentUser = JSON.parse( window.localStorage.getItem( 'currentUser' ) );

$$( '.app_name' ).text( app_name );

document.addEventListener( 'backbutton', function() {
	$$( '.back' ).click();
}, false);

document.addEventListener( 'deviceready', function() {
	if ( window.localStorage.getItem( 'account_setup' ) == null )
		setup_account();

	else {
		if ( currentUser.id == null )
			open_login_screen();

		load_dashboard();
	}
}, false );

function ksort( object ){
	var keys = Object.keys( object ).sort(),
		sorted_object = {};

	for ( var i in keys )
		sorted_object[ keys[ i ] ] = object[ keys[ i ] ];

	return Object.values( sorted_object );
}

function random_string( length ) {
    var charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
		string = '';

    for ( var i = 0; i < length; i++ ) {
        var position = Math.floor( Math.random() * charset.length );

        string += charset.substring( position, position + 1 );
    }

    return string;
}


function setup_account() {
	app.popup( '.popup-account', false, true );

	$$( '#account_submit' ).off( 'click' ).on( 'click', function() {
		var account_name = $$( '#account_name' );

		if ( account_name.val() == '' )
			account_name.focus();

		else {
			app.showPreloader();

			$$.post(
				api_base + 'account/' + account_name.val(),
				{},
				function( response ) {
					var response = JSON.parse( response );

					if ( response.result == 'successful' ) {
						window.localStorage.setItem( 'account_id', response.account_id );

						create_database_tables();
					}

					else {
						app.hidePreloader();

						app.closeModal( '.popup-account', true );

						app.alert( 'Account not known. Try again!' );
					}
				},
				function() {
					app.hidePreloader();

					app.alert( 'Server not accessible. Try again later!' );
				}
			);
		}
	} );
}

function create_database_tables() {
	db.transaction( function( tx ) {
		tx.executeSql( 'CREATE TABLE users ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, username CHAR(20), password CHAR(60) )' );
		tx.executeSql( 'CREATE TABLE user_meta ( id INTEGER PRIMARY KEY AUTOINCREMENT, user_id INTEGER, meta_key CHAR(250), meta_value TEXT )' );
		tx.executeSql( 'CREATE TABLE taxonomies ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, singular CHAR(20), plural CHAR(30) )' );
		tx.executeSql( 'CREATE TABLE terms ( id INTEGER PRIMARY KEY AUTOINCREMENT, taxonomy_id INTEGER, remote_id INTEGER, name CHAR(20) )' );
		tx.executeSql( 'CREATE TABLE term_meta ( id INTEGER PRIMARY KEY AUTOINCREMENT, term_id INTEGER, meta_key CHAR(40), meta_value TEXT )' );
		tx.executeSql( 'CREATE TABLE post_types ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, singular CHAR(40), plural CHAR(45) )' );
		tx.executeSql( 'CREATE TABLE posts ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, post_type_id INTEGER, parent_id INTEGER, name CHAR(50) )' );
		tx.executeSql( 'CREATE TABLE post_meta ( id INTEGER PRIMARY KEY AUTOINCREMENT, post_id INTEGER, meta_key CHAR(40), meta_value TEXT )' );
	} );

	setTimeout( function() {
		open_login_screen();

		download_form_element_types();
	}, 1000 );
}

function update_term( taxonomy_id, name, term_meta, remote_id ) {
	term_meta = typeof term_meta == 'undefined' ? {} : term_meta;
	remote_id = typeof remote_id == 'undefined' ? null : remote_id;

	db.transaction( function( tx ){
		tx.executeSql( 'SELECT id FROM terms WHERE taxonomy_id = ? AND name = ? LIMIT 1', [ taxonomy_id, name ], function( tx, result ) {
			if ( result.rows.length == 1 ) {
				var term_id = result.rows.item( 0 ).id;

				tx.executeSql( 'UPDATE terms SET remote_id = ? WHERE id = ?', [ remote_id, term_id ] );

				$$.each( term_meta, function( meta_key, meta_value ) {
					update_term_meta( term_id, meta_key, meta_value );
				} );
			}

			else
			    tx.executeSql( 'INSERT INTO terms ( taxonomy_id, name, remote_id ) VALUES( ?, ?, ? )',[ taxonomy_id, name, remote_id ], function( tx, result ) {
					var term_id =  result.insertId;

					$$.each( term_meta, function( meta_key, meta_value ) {
						update_term_meta( term_id, meta_key, meta_value );
					} );
				} );
		} );
	} );
}

function update_term_meta( term_id, meta_key, meta_value ) {
	db.transaction( function( tx ){
		tx.executeSql( 'SELECT id FROM term_meta WHERE term_id = ? AND meta_key = ? LIMIT 1', [ term_id, meta_key ], function( tx, result ) {
			if ( result.rows.length == 1 )
				tx.executeSql( 'UPDATE term_meta SET meta_value = ? WHERE term_id = ? AND meta_key = ?', [ meta_value, term_id, meta_key ] );

			else
				tx.executeSql( 'INSERT INTO term_meta ( term_id, meta_key, meta_value ) VALUES( ?, ?, ? )', [ term_id,  meta_key, meta_value ] );
		} );
	});
}

function update_post( post_type_id, name, post_meta, remote_id, parent_id ) {
	name = typeof name == 'undefined' ? ( new Date() ).getTime() : name;
	post_meta = typeof post_meta == 'undefined' ? {} : post_meta;
	remote_id = typeof remote_id == 'undefined' ? null : remote_id;
	parent_id = typeof parent_id == 'undefined' ? null : parent_id;

	db.transaction( function( tx ) {
		if ( remote_id == null ) {
			var query = 'SELECT id FROM posts WHERE post_type_id = ? AND name = ? LIMIT 1',
				params = [ post_type_id, name ];
		}

		else {
			var query = 'SELECT id FROM posts WHERE post_type_id = ? AND name = ? AND remote_id = ? LIMIT 1',
				params = [ post_type_id, name, remote_id ];
		}

		tx.executeSql( query, params, function( tx, result ) {
			if ( result.rows.length == 1 ) {
				var post_id = result.rows.item( 0 ).id;

				tx.executeSql( 'UPDATE posts SET remote_id = ? WHERE id = ?', [ remote_id, post_id ] );

				$$.each( post_meta, function( meta_key, meta_value ) {
					update_post_meta( post_id, meta_key, meta_value );
				} );
			}

			else
			    tx.executeSql( 'INSERT INTO posts ( post_type_id, name, parent_id, remote_id ) VALUES( ?, ?, ?, ? )',[ post_type_id, name, parent_id, remote_id ], function( tx, result ) {
					var post_id =  result.insertId;

					$$.each( post_meta, function( meta_key, meta_value ) {
						update_post_meta( post_id, meta_key, meta_value );
					} );
				} );
		} );
	} );
}

function update_post_meta( post_id, meta_key, meta_value ) {
	db.transaction( function( tx ) {
		tx.executeSql( 'SELECT id FROM post_meta WHERE post_id = ? AND meta_key = ? LIMIT 1', [ post_id, meta_key ], function( tx, result ) {
			if ( result.rows.length == 1 )
				tx.executeSql( 'UPDATE post_meta SET meta_value = ? WHERE post_id = ? AND meta_key = ?', [ meta_value, post_id, meta_key ] );

			else
				tx.executeSql( 'INSERT INTO post_meta ( post_id, meta_key, meta_value ) VALUES( ?, ?, ? )', [ post_id,  meta_key, meta_value ] );
		} );
	});
}

function update_user_meta( user_id, meta_key, meta_value ) {
	user_id = typeof user_id == 'undefined' ? 0 : parseInt( user_id );
	meta_value = typeof meta_value == 'undefined' ? '' : meta_value;

	if ( typeof meta_key != 'undefined' ) {
		db.transaction( function( tx ) {
			tx.executeSql( 'SELECT id FROM user_meta WHERE user_id = ? AND meta_key = ? LIMIT 1', [ user_id, meta_key ], function( tx, result ) {
				if ( result.rows.length == 1 )
					tx.executeSql( 'UPDATE user_meta SET meta_value = ? WHERE id = ?', [ meta_value, user_id ] );

				else
					tx.executeSql( 'INSERT INTO user_meta ( user_id, meta_key, meta_value ) VALUES ( ?, ?, ? )', [ user_id, meta_key, meta_value ] );
			} );
		} );
	}
}


function download_form_element_types() {
	$$.post(
		api_base + 'account/' + window.localStorage.getItem( 'account_id' ) + '/form-element-types',
		{},
		function( response ) {
			var response = JSON.parse( response );

			if ( response.result == 'successful' ) {
				if ( response.form_element_types.length == 0 ) {
					app.hidePreloader();

					app.alert( 'Setup cannot continue. No form element types found.' );

					setTimeout( function() {
						navigator.app.exitApp();
					}, 5000 );
				}

				var taxonomy = response.taxonomy;

				db.transaction( function( tx ) {
					tx.executeSql( 'SELECT id FROM taxonomies WHERE singular = ? LIMIT 1', [ taxonomy.name.singular ], function( tx, result ) {
						if ( result.rows.length == 1 )
							$$.each( response.form_element_types, function( index, form_element_type ) {
								update_term( result.rows.item( 0 ).id, form_element_type.name, {
									blueprint: form_element_type.blueprint
								}, form_element_type.id );

								if ( index == ( response.form_element_types.length - 1 ) )
									download_users();
							} );

						else
							tx.executeSql( 'INSERT INTO taxonomies ( remote_id, singular, plural ) VALUES ( ?, ?, ? )', [ taxonomy.id, taxonomy.name.singular, taxonomy.name.plural ], function( tx, result ) {
								$$.each( response.form_element_types, function( index, form_element_type ) {
									update_term( result.insertId, form_element_type.name, {
										blueprint: form_element_type.blueprint
									}, form_element_type.id );

									if ( index == ( response.form_element_types.length - 1 ) )
										download_users();
								} );
							} );
					} );
				} );
			}

			else
				download_form_element_types();
		},
		function() {
			download_form_element_types();
		}
	);
}

function download_users() {
	if ( pagination.page == 1 ) {
		truncate_table( 'users' );
		truncate_table( 'user_meta' );
	}

	$$.post(
		api_base + 'account/' + window.localStorage.getItem( 'account_id' ) + '/users',
		{
			per_page: pagination.per_page,
			page: pagination.page
		},
		function( response ) {
			var response = JSON.parse( response );

			if ( response.result == 'successful' ) {
				if ( response.users.length == 0 && pagination.page == 1 ) {
					app.hidePreloader();

					app.alert( 'Setup cannot continue. No users found.' );

					setTimeout( function() {
						navigator.app.exitApp();
					}, 5000 );
				}

				else if ( response.users.length == 0 && pagination.page > 1 ) {
					pagination.page = 1;

					download_modules();
				}

				db.transaction( function( tx ) {
					$$.each( response.users, function( index, user ) {
						if ( user.meta.has_mobile_app_access == '1' ) {
							tx.executeSql(
								'INSERT INTO users ( remote_id, username, password ) VALUES ( ?, ?, ? )',
								[ user.id, user.username, user.meta.mobile_app_password ],
								function( tx, result ) {
									var user_meta = Object.assign( {
											first_name: user.name.first,
											last_name: user.name.last
										}, user.meta ),
										user_id = result.insertId;

									$$.each( user_meta, function( meta_key, meta_value ) {
										update_user_meta( user_id, meta_key, meta_value );
									} );
								}
							);
						}

						if ( index == ( response.users.length - 1 ) ) {
							pagination.page = ( response.users.length < pagination.per_page ) ? 1 : ( pagination.page + 1 );

							if ( pagination.per_page == response.users.length )
								download_users();

							else
								download_modules();
						}
					} );
				} );
			}

			else
				download_users();
		},
		function() {
			download_users();
		}
	);
}


function download_modules() {
	$$.post(
		api_base + 'account/' + window.localStorage.getItem( 'account_id' ) + '/modules',
		{
			account: window.localStorage.getItem( 'account_id' ),
			per_page: pagination.per_page,
			page: pagination.page
		},
		function( response ) {
			var response = JSON.parse( response );

			if ( response.result == 'successful' ) {
				if ( response.modules.length == 0 && pagination.page == 1 ) {
					app.hidePreloader();

					app.alert( 'Setup cannot continue. No modules found.' );

					setTimeout( function() {
						navigator.app.exitApp();
					}, 5000 );
				}

				else if ( response.modules.length == 0 && pagination.page > 1 ) {
					pagination.page = 1;

					download_forms();
				}

				var taxonomy = response.taxonomy;

				db.transaction( function( tx ) {
					tx.executeSql( 'SELECT id FROM taxonomies WHERE singular = ? LIMIT 1', [ taxonomy.name.singular ], function( tx, result ) {
						if ( result.rows.length == 1 )
							$$.each( response.modules, function( index, module ) {
								update_term( result.rows.item( 0 ).id, module.name, module.meta, module.id );

								if ( index == ( response.modules.length - 1 ) ) {
									pagination.page = ( response.modules.length < pagination.per_page ) ? 1 : ( pagination.page + 1 );

									if ( pagination.per_page == response.modules.length )
										download_modules();

									else
										download_forms();
								}
							} );

						else
							tx.executeSql( 'INSERT INTO taxonomies ( remote_id, singular, plural ) VALUES ( ?, ?, ? )', [ taxonomy.id, taxonomy.name.singular, taxonomy.name.plural ], function( tx, result ) {
								$$.each( response.modules, function( index, module ) {
									update_term( result.insertId, module.name, module.meta, module.id );

									if ( index == ( response.modules.length - 1 ) ) {
										pagination.page = ( response.modules.length < pagination.per_page ) ? 1 : ( pagination.page + 1 );

										if ( pagination.per_page == response.modules.length )
											download_modules();

										else
											download_forms();
									}
								} );
							} );
					} );
				} );
			}

			else
				download_modules();
		},
		function() {
			download_modules();
		}
	);
}

function download_forms() {
	var modules_loaded = false,
		modules = [];

	db.transaction( function( tx ) {
		tx.executeSql( 'SELECT terms.id AS id, terms.remote_id AS remote_id FROM terms LEFT JOIN taxonomies ON terms.taxonomy_id = taxonomies.id WHERE taxonomies.singular = ?', [ 'Module' ], function( tx, result ) {
			for ( i = 0; i < result.rows.length; i++ )
				modules.push( {
					id: result.rows.item( i ).id,
					remote_id: result.rows.item( i ).remote_id
				} );

			setTimeout( function() {
				modules_loaded = true;
			}, 100 );
		} );
	} );

	var modules_loaded_interval = setInterval( function() {
		if ( modules_loaded ) {
			clearInterval( modules_loaded_interval );

			var active_module_index = 0,
				module_actively_downloading = false;

			var module_downloading_interval = setInterval( function() {
				if ( module_actively_downloading == false ) {
					module_actively_downloading = true;

					if ( typeof modules[ active_module_index ] != 'undefined' )
						$$.post(
							api_base + 'module/' + modules[ active_module_index ].remote_id + '/forms',
							{},
							function( response ) {
								var response = JSON.parse( response );

								if ( response.result == 'successful' ) {
									var post_type = response.post_type;

									db.transaction( function( tx ) {
										tx.executeSql( 'SELECT id FROM post_types WHERE singular = ? LIMIT 1', [ post_type.name.singular ], function( tx, result ) {
											if ( result.rows.length == 1 ) {
												post_type_id = result.rows.item( 0 ).id;

												if ( response.forms.length == 0 ) {
													active_module_index++;

													module_actively_downloading = false;
												}

												else
													$$.each( response.forms, function( index, form ) {
														update_post( post_type_id, form.name, {
															module: parseInt( modules[ active_module_index ].id )
														}, form.id );

														if ( index ==  ( response.forms.length - 1 ) ) {
															active_module_index++;

															module_actively_downloading = false;
														}
													} );
											}

											else
												tx.executeSql( 'INSERT INTO post_types ( remote_id, singular, plural ) VALUES ( ?, ?, ? )', [ post_type.id, post_type.name.singular, post_type.name.plural ], function( tx, result ) {
													post_type_id = result.insertId;

													if ( response.forms.length == 0 ) {
														active_module_index++;

														module_actively_downloading = false;
													}

													else
														$$.each( response.forms, function( index, form ) {
															update_post( post_type_id, form.name, {
																module: parseInt( modules[ active_module_index ].id )
															}, form.id );

															if ( index ==  ( response.forms.length - 1 ) ) {
																active_module_index++;

																module_actively_downloading = false;
															}
														} );
												} );
										} );
									} );
								}

								else
									module_actively_downloading = false;
							},
							function() {
								module_actively_downloading = false;
							}
						);

					else {
						clearInterval( module_downloading_interval );

						download_form_sections();
					}
				}
			}, 50 );
		}
	}, 50 );
}

function download_form_sections() {
	var forms_loaded = false,
		forms = [];

	db.transaction( function( tx ) {
		tx.executeSql( 'SELECT posts.id AS id, posts.remote_id AS remote_id FROM posts LEFT JOIN post_types ON posts.post_type_id = post_types.id WHERE post_types.singular = ?', [ 'Form' ], function( tx, result ) {
			for ( i = 0; i < result.rows.length; i++ )
				forms.push( {
					id: result.rows.item( i ).id,
					remote_id: result.rows.item( i ).remote_id
				} );

			setTimeout( function() {
				forms_loaded = true;
			}, 100 );
		} );
	} );

	var forms_loaded_interval = setInterval( function() {
		if ( forms_loaded ) {
			clearInterval( forms_loaded_interval );

			var active_form_index = 0,
				form_actively_downloading = false;

			var form_downloading_interval = setInterval( function() {
				if ( form_actively_downloading == false ) {
					form_actively_downloading = true;

					if ( typeof forms[ active_form_index ] != 'undefined' )
						$$.post(
							api_base + 'form/' + forms[ active_form_index ].remote_id + '/sections',
							{
								per_page: pagination.per_page,
								page: pagination.page
							},
							function( response ) {
								var response = JSON.parse( response );

								if ( response.result == 'successful' ) {
									var post_type = response.post_type;

									db.transaction( function( tx ) {
										tx.executeSql( 'SELECT id FROM post_types WHERE singular = ? LIMIT 1', [ post_type.name.singular ], function( tx, result ) {
											if ( result.rows.length == 1 ) {
												post_type_id = result.rows.item( 0 ).id;

												if ( response.form_sections.length == 0 ) {
													active_form_index++;

													pagination.page = 1;

													form_actively_downloading = false;
												}

												else {
													$$.each( response.form_sections, function( index, form_section ) {
														update_post( post_type_id, form_section.name, Object.assign( {
															order: form_section.order,
															hidden: form_section.hidden
														} ), form_section.id, forms[ active_form_index ].id );
													} );

													if ( pagination.per_page == response.form_sections.length )
														pagination.page++;

													else {
														active_form_index++;

														pagination.page = 1;
													}

													form_actively_downloading = false;
												}
											}

											else
												tx.executeSql( 'INSERT INTO post_types ( remote_id, singular, plural ) VALUES ( ?, ?, ? )', [ post_type.id, post_type.name.singular, post_type.name.plural ], function( tx, result ) {
													post_type_id = result.insertId;

													if ( response.form_sections.length == 0 ) {
														active_form_index++;

														pagination.page = 1;

														form_actively_downloading = false;
													}

													else {
														$$.each( response.form_sections, function( index, form_section ) {
															update_post( post_type_id, form_section.name, Object.assign( {
																order: form_section.order,
																hidden: form_section.hidden
															} ), form_section.id, forms[ active_form_index ].id );
														} );

														if ( pagination.per_page == response.form_sections.length )
															pagination.page++;

														else {
															active_form_index++;

															pagination.page = 1;
														}

														form_actively_downloading = false;
													}
												} );
										} );
									} );
								}

								else
									form_actively_downloading = false;
							},
							function() {
								form_actively_downloading = false;
							}
						);

					else {
						download_section_elements();
					}
				}
			}, 50 );
		}
	}, 50 );
}

function download_section_elements() {
	var form_sections_loaded = false,
		form_sections = [];

	db.transaction( function( tx ) {
		tx.executeSql( 'SELECT posts.id AS id, posts.remote_id AS remote_id FROM posts LEFT JOIN post_types ON posts.post_type_id = post_types.id WHERE post_types.singular = ?', [ 'Form section' ], function( tx, result ) {
			for ( i = 0; i < result.rows.length; i++ )
				form_sections.push( {
					id: result.rows.item( i ).id,
					remote_id: result.rows.item( i ).remote_id
				} );

			setTimeout( function() {
				form_sections_loaded = true;
			}, 100 );
		} );
	} );

	var form_sections_loaded_interval = setInterval( function() {
		if ( form_sections_loaded ) {
			clearInterval( form_sections_loaded_interval );

			var active_form_section_index = 0,
				form_section_actively_downloading = false;

			var form_section_downloading_interval = setInterval( function() {
				if ( form_section_actively_downloading == false ) {
					form_section_actively_downloading = true;

					if ( typeof form_sections[ active_form_section_index ] != 'undefined' )
						$$.post(
							api_base + 'form-section/' + form_sections[ active_form_section_index ].remote_id + '/elements',
							{
								per_page: pagination.per_page,
								page: pagination.page
							},
							function( response ) {
								var response = JSON.parse( response );

								if ( response.result == 'successful' ) {
									var post_type = response.post_type;

									db.transaction( function( tx ) {
										tx.executeSql( 'SELECT id FROM post_types WHERE singular = ? LIMIT 1', [ post_type.name.singular ], function( tx, result ) {
											if ( result.rows.length == 1 ) {
												post_type_id = result.rows.item( 0 ).id;

												if ( response.section_elements.length == 0 ) {
													active_form_section_index++;

													pagination.page = 1;

													form_section_actively_downloading = false;
												}

												else {
													$$.each( response.section_elements, function( index, section_element ) {
														update_post( post_type_id, section_element.name, Object.assign( {
															type: section_element.type
														}, section_element.meta_data ), section_element.id, form_sections[ active_form_section_index ].id );
													} );

													if ( pagination.per_page == response.section_elements.length )
														pagination.page++;

													else {
														active_form_section_index++;

														pagination.page = 1;
													}

													form_section_actively_downloading = false;
												}
											}

											else
												tx.executeSql( 'INSERT INTO post_types ( remote_id, singular, plural ) VALUES ( ?, ?, ? )', [ post_type.id, post_type.name.singular, post_type.name.plural ], function( tx, result ) {
													post_type_id = result.insertId;

													if ( response.section_elements.length == 0 ) {
														active_form_section_index++;

														pagination.page = 1;

														form_section_actively_downloading = false;
													}

													else {
														$$.each( response.section_elements, function( index, section_element ) {
															update_post( post_type_id, section_element.name, Object.assign( {
																type: section_element.type
															}, section_element.meta_data ), section_element.id, form_sections[ active_form_section_index ].id );
														} );

														if ( pagination.per_page == response.section_elements.length )
															pagination.page++;

														else {
															active_form_section_index++;

															pagination.page = 1;
														}

														form_section_actively_downloading = false;
													}
												} );
										} );
									} );
								}

								else
									form_section_actively_downloading = false;
							},
							function() {
								form_section_actively_downloading = false;
							}
						);

					else {
						download_form_submissions();
					}
				}
			}, 50 );
		}
	}, 50 );
}

function download_form_submissions() {
	var forms_loaded = false,
		forms = [];

	db.transaction( function( tx ) {
		tx.executeSql( 'SELECT posts.id AS id, posts.remote_id AS remote_id FROM posts LEFT JOIN post_types ON posts.post_type_id = post_types.id WHERE post_types.singular = ?', [ 'Form' ], function( tx, result ) {
			for ( i = 0; i < result.rows.length; i++ )
				forms.push( {
					id: result.rows.item( i ).id,
					remote_id: result.rows.item( i ).remote_id
				} );

			setTimeout( function() {
				forms_loaded = true;
			}, 100 );
		} );
	} );

	var forms_loaded_interval = setInterval( function() {
		if ( forms_loaded ) {
			clearInterval( forms_loaded_interval );

			var active_form_index = 0,
				form_actively_downloading = false;

			var form_downloading_interval = setInterval( function() {
				if ( form_actively_downloading == false ) {
					form_actively_downloading = true;

					if ( typeof forms[ active_form_index ] != 'undefined' )
						$$.post(
							api_base + 'form/' + forms[ active_form_index ].remote_id + '/submissions',
							{
								per_page: pagination.per_page,
								page: pagination.page
							},
							function( response ) {
								var response = JSON.parse( response );

								if ( response.result == 'successful' ) {
									var post_type = response.post_types.submission;

									db.transaction( function( tx ) {
										tx.executeSql( 'SELECT id FROM post_types WHERE singular = ? LIMIT 1', [ post_type.name.singular ], function( tx, result ) {
											if ( result.rows.length == 1 ) {
												post_type_id = result.rows.item( 0 ).id;

												if ( response.form_submissions.length == 0 ) {
													active_form_index++;

													pagination.page = 1;

													form_actively_downloading = false;
												}

												else {
													$$.each( response.form_submissions, function( index, submission ) {
														update_post( post_type_id, submission.name, Object.assign( {
															timestamp: submission.timestamp
														} ), submission.id, forms[ active_form_index ].id );
													} );

													if ( pagination.per_page == response.form_submissions.length )
														pagination.page++;

													else {
														active_form_index++;

														pagination.page = 1;
													}

													form_actively_downloading = false;
												}
											}

											else
												tx.executeSql( 'INSERT INTO post_types ( remote_id, singular, plural ) VALUES ( ?, ?, ? )', [ post_type.id, post_type.name.singular, post_type.name.plural ], function( tx, result ) {
													post_type_id = result.insertId;

													if ( response.form_submissions.length == 0 ) {
														active_form_index++;

														pagination.page = 1;

														form_actively_downloading = false;
													}

													else {
														$$.each( response.form_submissions, function( index, submission ) {
															update_post( post_type_id, submission.name, Object.assign( {
																timestamp: submission.timestamp
															} ), submission.id, forms[ active_form_index ].id );
														} );

														if ( pagination.per_page == response.form_submissions.length )
															pagination.page++;

														else {
															active_form_index++;

															pagination.page = 1;
														}

														form_actively_downloading = false;
													}
												} );
										} );
									} );

									db.transaction( function( tx ) {
										tx.executeSql( 'SELECT id FROM post_types WHERE singular = ? LIMIT 1', [ response.post_types.response.name.singular ], function( tx, result ) {
											if ( result.rows.length != 1 )
												tx.executeSql( 'INSERT INTO post_types ( remote_id, singular, plural ) VALUES ( ?, ?, ? )', [ response.post_types.response.id, response.post_types.response.name.singular, response.post_types.response.name.plural ] );
										} );
									} );
								}

								else
									form_actively_downloading = false;
							},
							function() {
								form_actively_downloading = false;
							}
						);

					else {
						download_submission_responses();
					}
				}
			}, 50 );
		}
	}, 50 );
}

function download_submission_responses() {
	var form_submissions_loaded = false,
		form_submissions = [];

	db.transaction( function( tx ) {
		tx.executeSql( 'SELECT posts.id AS id, posts.remote_id AS remote_id FROM posts LEFT JOIN post_types ON posts.post_type_id = post_types.id WHERE post_types.singular = ?', [ 'Form submission' ], function( tx, result ) {
			for ( i = 0; i < result.rows.length; i++ )
				form_submissions.push( {
					id: result.rows.item( i ).id,
					remote_id: result.rows.item( i ).remote_id
				} );

			setTimeout( function() {
				form_submissions_loaded = true;
			}, 100 );
		} );
	} );

	var form_submissions_loaded_interval = setInterval( function() {
		if ( form_submissions_loaded ) {
			clearInterval( form_submissions_loaded_interval );

			var active_form_submission_index = 0,
				form_submission_actively_downloading = false;

			var form_submission_downloading_interval = setInterval( function() {
				if ( form_submission_actively_downloading == false ) {
					form_submission_actively_downloading = true;

					if ( typeof form_submissions[ active_form_submission_index ] != 'undefined' )
						$$.post(
							api_base + 'form-submission/' + form_submissions[ active_form_submission_index ].remote_id + '/responses',
							{
								per_page: pagination.per_page,
								page: pagination.page
							},
							function( response ) {
								var response = JSON.parse( response );

								if ( response.result == 'successful' ) {
									var post_type = response.post_type;

									db.transaction( function( tx ) {
										tx.executeSql( 'SELECT id FROM post_types WHERE singular = ? LIMIT 1', [ post_type.name.singular ], function( tx, result ) {
											if ( result.rows.length == 1 ) {
												post_type_id = result.rows.item( 0 ).id;

												if ( response.responses.length == 0 ) {
													active_form_submission_index++;

													pagination.page = 1;

													form_submission_actively_downloading = false;
												}

												else {
													$$.each( response.responses, function( index, form_response ) {
														update_post( post_type_id, form_response.name, Object.assign( {
															element: form_response.form_element,
															response: form_response.response
														} ), form_response.id, form_submissions[ active_form_submission_index ].id );
													} );

													if ( pagination.per_page == response.responses.length )
														pagination.page++;

													else {
														active_form_submission_index++;

														pagination.page = 1;
													}

													form_submission_actively_downloading = false;
												}
											}

											else
												tx.executeSql( 'INSERT INTO post_types ( remote_id, singular, plural ) VALUES ( ?, ?, ? )', [ post_type.id, post_type.name.singular, post_type.name.plural ], function( tx, result ) {
													post_type_id = result.insertId;

													if ( response.section_elements.length == 0 ) {
														active_form_submission_index++;

														pagination.page = 1;

														form_submission_actively_downloading = false;
													}

													else {
														$$.each( response.responses, function( index, form_response ) {
															update_post( post_type_id, form_response.name, Object.assign( {
																element: form_response.form_element,
																response: form_response.response
															} ), form_response.id, form_submissions[ active_form_submission_index ].id );
														} );

														if ( pagination.per_page == response.responses.length )
															pagination.page++;

														else {
															active_form_submission_index++;

															pagination.page = 1;
														}

														form_submission_actively_downloading = false;
													}
												} );
										} );
									} );
								}

								else
									form_submission_actively_downloading = false;
							},
							function() {
								form_submission_actively_downloading = false;
							}
						);

					else {
						clearInterval( form_submission_downloading_interval );

						window.localStorage.setItem( 'account_setup', 1 );

						open_login_screen();

						setTimeout( function() {
							app.hidePreloader();

							app.closeModal( '.popup-account', true );
						}, 500 );
					}
				}
			}, 50 );
		}
	}, 50 );
}

function truncate_table( table ) {
	db.transaction( function( tx ) {
		tx.executeSql( 'DELETE FROM ' + table, [] );
	} );
}

function show_table_records( table ) {
	table = typeof table == 'undefined' ? 'users' : table;

	db.transaction( function( tx ) {
		tx.executeSql( 'SELECT * FROM ' + table, [], function( tx, result ) {
			for ( i = 0; i < result.rows.length; i++ )
				console.log( result.rows.item( i ) );
		} );
	} )
}


function open_login_screen() {
	var count = 0,
		login_form_interval = setInterval( function() {
			if ( count > 10000 )
				clearInterval( login_form_interval );

			$$( '.login-screen-content > form' ).css( {
				'margin-top' : parseInt( ( $$( window ).height() - 220 ) / 2 ) + 'px'
			} );
		}, 100 );

	app.loginScreen( '.login-screen', true );

	$$( '#login_submit' ).off( 'click' ).on( 'click', function( event ) {
		var username = $$( '#username' ),
			password = $$( '#password' );

		if ( username.val().trim().length == 0 )
			username.focus();

		else if ( password.val().trim().length == 0 )
			password.focus();

		else {
			app.showPreloader();

			db.transaction( function( tx ) {
				tx.executeSql( 'SELECT * FROM users WHERE username = ? LIMIT 1', [ username.val() ], function( tx, result ) {
					if ( result.rows.length > 0 ) {
						tx.executeSql( 'SELECT * FROM users WHERE username = ? AND password = ? LIMIT 1', [ username.val(), password.val() ], function( tx, result ) {
							if ( result.rows.length > 0 ) {
								var record = result.rows.item( 0 );

								currentUser.id = record.id;
								currentUser.remote_id =  record.remote_id;
								currentUser.username = record.username;
								currentUser.password = record.password;

								window.localStorage.setItem( 'currentUser', JSON.stringify( currentUser ) );

								load_dashboard();

								app.hidePreloader();

								setTimeout( function() {
									app.closeModal( '.login-screen', true );
								}, 500 );
							}

							else {
								password.val( '' ).focus();

								app.hidePreloader();

								app.alert( 'Wrong password entered. Try again!' );
							}
						} );
					}

					else {
						username.val( '' ).focus();
						password.val( '' );

						app.alert( 'Wrong username entered. Try again! ' );
					}
				} );
			} );
		}
	} );
}

function load_dashboard() {
	start_automatic_synchronization();

	db.transaction( function( tx ) {
		tx.executeSql( 'SELECT id FROM taxonomies WHERE singular = ? LIMIT 1', [ 'Module' ], function( tx, result ) {
			if ( result.rows.length == 1 ) {
				var taxonomy_id = result.rows.item( 0 ).id,
					terms_loaded = false,
					modules = [];

				tx.executeSql( 'SELECT id, name FROM terms WHERE taxonomy_id = ? ORDER BY name ASC', [ taxonomy_id ], function( tx, result ) {
					for ( i = 0; i < result.rows.length; i++ )
						modules.push( {
							id: result.rows.item( i ).id,
							name: result.rows.item( i ).name
						} );

					setTimeout( function() {
						terms_loaded = true;
					}, 50 );
				} );

				var terms_loaded_interval = setInterval( function() {
					if ( terms_loaded_interval ) {
						clearInterval( terms_loaded_interval );

						db.transaction( function( tx ) {
							var term_ids = [];

							$$.each( modules, function( index, module ) {
								term_ids.push( module.id );

								if ( modules.length == term_ids.length ) {
									db.transaction( function( tx ) {
										tx.executeSql( 'SELECT id, term_id, meta_key, meta_value FROM term_meta WHERE term_id IN ( ' + term_ids.join( ', ' ) + ' )', [], function( tx, result ) {
											$$.each( modules, function( index, module ) {
												var module_id = module.id;

												for ( i = 0; i < result.rows.length; i++ ) {
													var record = result.rows.item( i );

													if ( record.term_id == module_id ) {
														var object = {};
															object[ record.meta_key ] = record.meta_value;

														modules[ index ] = Object.assign( {}, object, modules[ index ] );
													}
												}

												if ( index == ( modules.length - 1 ) ) {
													var ordered_modules = [];

													$$.each( modules, function( index, module ) {
														ordered_modules[ module.order ] = {
															id: module.id,
															icon: module.icon,
															name: module.name
														};
													} );

													ordered_modules = ksort( ordered_modules );

													ordered_modules = ordered_modules.filter( function( element ) {
														return element != undefined
													} );

													var html = '';

													$$.each( ordered_modules, function( index, module ) {
														counter = index + 1;

														if ( counter % 2 == 1 )
															html += '<div class="row no-gutter">';

															html += '<a href="pages/module.html" data-module-id="' + module.id + '" class="col-50 button">' +
																'<i class="f7-icons">' + module.icon + '</i>' +
																'<span>' +
																	module.name +
																'</span>' +
															 '</a>';

														if ( counter == result.rows.length || counter % 2 == 0 )
															html += '</div>';
													} );

													$$( '#modules_list' ).html( html );

													$$( '#modules_list a' ).on( 'click', function() {
														current_module_id = $$( this ).attr( 'data-module-id' );
													} );
												}
											} );
										} );
									} );
								}
							} );
						} );
					}
				}, 50 );
			}
		} );
	} );
}

$$( document ).on( 'page:beforeinit', '.page[data-page="module"]', function( event ) {
	db.transaction( function( tx ) {
		tx.executeSql( 'SELECT name FROM terms WHERE id = ? LIMIT 1', [ current_module_id ], function( tx, result ) {
			$$( '.module_name' ).text( result.rows.item( 0 ).name );

			tx.executeSql(
				'SELECT DISTINCT posts.id AS id, posts.name AS name FROM posts LEFT JOIN post_meta ON posts.id = post_meta.post_id WHERE post_meta.meta_key = ? AND post_meta.meta_value = ? ORDER BY posts.name ASC',
				[ 'module', '' + current_module_id + '.0' ],
				function( tx, result ) {
					var html = '';

					for ( i = 0; i < result.rows.length; i++ ) {
						var record = result.rows.item( i );

						html += '<li>' +
							'<a href="#" data-form-id="' + record.id + '" class="item-link item-content">' +
								'<div class="item-inner">' +
									'<div class="item-title">' + record.name + '</div>' +
								'</div>' +
							'</a>' +
						'</li>';
					}

					$$( '.module-forms-list-block ul' ).html( html );

					$$( '.module-forms-list-block ul li a' ).on( 'click', function( event ) {
						current_form_id = $$( this ).attr( 'data-form-id' );

						load_form_page();
					} );
				}
			);
		} );
	} )
} );

function load_form_page() {
	app.showPreloader();

	var page_html = '<div data-page="form" class="page toolbar-fixed navbar-fixed">';

	db.transaction( function( tx ) {
		tx.executeSql( 'SELECT name FROM posts WHERE id = ? LIMIT 1', [ current_form_id ], function( tx, result ) {
			page_html += '<div class="navbar">' +
				'<div class="navbar-inner">' +
					'<div class="left">' +
						'<a href="#" class="back link">' +
							'<i class="f7-icons">chevron_left</i>' +
						'</a>' +
					'</div>' +
					'<div class="center form_name">' +
						result.rows.item( 0 ).name +
					'</div>' +
				'</div>' +
			'</div>';

			tx.executeSql( 'SELECT posts.id AS id, posts.name AS name, post_meta.meta_key, post_meta.meta_value FROM post_meta LEFT JOIN posts ON post_meta.post_id = posts.id WHERE posts.parent_id = ? AND posts.post_type_id = ( SELECT id FROM post_types WHERE singular = ? LIMIT 1 )', [ current_form_id, 'Form section' ], function( tx, result ) {
				var sections = [];

				for ( i = 0; i < result.rows.length; i++ ) {
					var record = result.rows.item( i );

					if ( typeof sections[ record.id ] == 'undefined' )
						sections[ record.id ] = {
							id: record.id,
							name: record.name,
							order: null,
							hidden: null
						};

					if ( record.meta_key == 'order' )
						sections[ record.id ].order = record.meta_value;

					else if ( record.meta_key == 'hidden' )
						sections[ record.id ].hidden = record.meta_value;
				}

				ordered_sections = [];

				$$.each( sections, function( index, section ) {
					if ( typeof section != 'undefined' )
						ordered_sections[ section.order ] = {
							id: section.id,
							name: section.name,
							hidden: section.hidden
						};
				} );

				sections = ksort( ordered_sections );

				page_html += '<div class="toolbar tabbar tabbar-scrollable">' +
					'<div class="toolbar-inner" id="form-tabs">';

				var active_tab = null;

				for ( i = 0; i < sections.length; i++ ) {
					if ( parseInt( sections[ i ].hidden ) != 1 && active_tab == null )
						active_tab = i;

					page_html += '<a href="#tab-' + sections[ i ].id + '" ' + ( parseInt( sections[ i ].hidden ) == 1 ? ' style="display: none;" ' : '' ) + ' class="tab-link ' + ( parseInt( sections[ i ].hidden ) == 1 ? ' disabled ' : '' ) + ( active_tab == i ? ' active ' : '' ) + '">' +
						sections[ i ].name +
					'</a>';
				}

				page_html += '</div>' +
				'</div>' +
				'<div class="tabs-swipeable-wrap">' +
					'<div class="tabs" id="form-tabs-content">';

				if ( sections.length > 0 ) {
					var section_index = 0,
						section_actively_rendering = false;

					var rendering_section_interval = setInterval( function() {
						if ( !section_actively_rendering ) {
							section_actively_rendering = true;

							var section = sections[ section_index ];

							page_html += '<div id="tab-' + section.id + '" ' + ( parseInt( section.hidden ) == 1 ? ' data-hide-tab="true" ' : ' data-hide-tab="false" ' ) + '  class="page-content app-form tab ' + ( parseInt( section.hidden ) == 1 ? ' disabled ' : '' ) + ( active_tab == section_index ? ' active ' : '' ) + '">' +
								'<div class="list-block no-margin form-elements">' +
									'<ul>';

							db.transaction( function( tx ) {
								tx.executeSql( 'SELECT id, name FROM posts WHERE parent_id = ?', [ section.id ], function( tx, result ) {
									var elements = [],
										post_ids = [];

									for ( i = 0; i < result.rows.length; i++ ) {
										elements.push( {
											id: result.rows.item( i ).id,
											name: result.rows.item( i ).name
										} );

										post_ids.push( result.rows.item( i ).id );
									}

									tx.executeSql( 'SELECT id, post_id, meta_key, meta_value FROM post_meta WHERE post_id IN ( ' + post_ids.join( ', ' ) + ' )', [], function( tx, result ) {
										$$.each( elements, function( index, element ) {
											var element_id = element.id;

											for ( i = 0; i < result.rows.length; i++ ) {
												var record = result.rows.item( i );

												if ( record.post_id == element_id ) {
													var object = {};
														object[ record.meta_key ] = record.meta_value;

													elements[ index ] = Object.assign( {}, object, elements[ index ] );
												}
											}

											if ( index == ( elements.length - 1 ) ) {
												var ordered_elements = [];

												$$.each( elements, function( index, element ) {
													ordered_elements[ parseInt( element.order ) ] = element;
												} );

												ordered_elements = ksort( ordered_elements );

												var fixed_element_types = false;

												db.transaction( function( tx ) {
													$$.each( ordered_elements, function( index, element ) {
														tx.executeSql( 'SELECT name FROM terms WHERE id = ? LIMIT 1', [ element.type ], function( tx, result ) {
															if ( result.rows.length == 1 )
																ordered_elements[ index ].type = result.rows.item( 0 ).name;

															if ( index == ( ordered_elements.length - 1 ) )
																fixed_element_types = true;
														} );
													} );
												} );

												var fixed_element_types_interval = setInterval( function() {
													if ( fixed_element_types ) {
														clearInterval( fixed_element_types_interval );

														var html = '';

														$$.each( ordered_elements, function( index, element ) {
															switch ( element.type ) {

																case 'checkbox' :

																	html += '<li ' + ( element.hidden == 1 || element.hidden == "true" ? 'style="display: none;"' : '' )  + '>' +
																		'<div class="item-content">' +
																			'<div class="item-inner">' +
									                                            '<div class="item-title label">' +
									                                                element.label +
									                                            '</div>' +
									                                            '<div class="item-input item-input-field">' +
																					'<div class="list-block no-margin checkbox-list" id="' + element.id + '">' +
																						'<ul>';

																							if ( element.dynamic_options == '0' || element.dynamic_options == 'false' ) {
																								var options = JSON.parse( element.options );

																								$$.each( options, function( value, label ) {
																									html += '<li>' +
																										'<label class="label-checkbox item-content">' +
																											'<input type="checkbox" value="' + value + '">' +
																											'<div class="item-media">' +
																												'<i class="icon icon-form-checkbox"></i>' +
																											'</div>' +
																											'<div class="item-inner">' +
																												'<div class="item-title">' +
																													label +
																												'</div>' +
																											'</div>' +
																										'</label>' +
																									'</li>';
																								} );
																							}

																						html += '</ul>' +
																					'</div>' +
									                                            '</div>' +
									                                        '</div>' +
																		'</div>' +
																	'</li>';

																	break;

																case 'text' :

																	html += '<li ' + ( element.hidden == 1 || element.hidden == "true" ? 'style="display: none;"' : '' )  + '>' +
																		'<div class="item-content">' +
																			'<div class="item-inner">' +
									                                            '<div class="item-title label">' +
									                                                element.label +
									                                            '</div>' +
									                                            '<div class="item-input item-input-field">' +
									                                                '<input type="text" ' + ( element.requred == '1' || element.required == 'true' ? ' required ' : '' ) + ' id="' + element.id + '" placeholder="' + element.name + '" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">' +
									                                            '</div>' +
									                                        '</div>' +
																		'</div>' +
																	'</li>';

																	break;

																case 'number' :

																	html += '<li ' + ( element.hidden == 1 || element.hidden == "true" ? 'style="display: none;"' : '' )  + '>' +
																		'<div class="item-content">' +
																			'<div class="item-inner">' +
									                                            '<div class="item-title label">' +
									                                                element.label +
									                                            '</div>' +
									                                            '<div class="item-input item-input-field">' +
									                                                '<input type="number" ' + ( element.requred == '1' || element.required == 'true' ? ' required ' : '' ) + ' id="' + element.id + '" placeholder="' + element.name + '" min="' + element.min + '" max="' + element.max + '" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">' +
									                                            '</div>' +
									                                        '</div>' +
																		'</div>' +
																	'</li>';

																	break;

																case 'textarea' :

																		html += '<li ' + ( element.hidden == 1 || element.hidden == "true" ? 'style="display: none;"' : '' )  + '>' +
																			'<div class="item-content">' +
																				'<div class="item-inner">' +
										                                            '<div class="item-title label">' +
										                                                element.label +
										                                            '</div>' +
										                                            '<div class="item-input item-input-field">' +
										                                                '<textarea ' + ( element.requred == '1' || element.required == 'true' ? ' required ' : '' ) + ' id="' + element.id + '" placeholder="' + element.name + '" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"></textarea>' +
										                                            '</div>' +
										                                        '</div>' +
																			'</div>' +
																		'</li>';

																		break;

																case 'flip-switch' :

																	html += '<li class="has-flip-switch" ' + ( element.hidden == 1 || element.hidden == "true" ? 'style="display: none;"' : '' )  + '>' +
																		'<div class="item-content">' +
																			'<div class="item-inner">' +
																				'<div class="item-title label">' +
																					element.label +
																				'</div>' +
																				'<div class="item-input item-input-field">' +
																					'<label class="label-switch inline-switch">' +
																						'<input type="checkbox" id="' + element.id + '" value="' + element.on_value + '" data-off-value="' + element.off_value + '">' +
																						'<div class="checkbox"></div>' +
																					'</label>' +
																				'</div>' +
																			'</div>' +
																		'</div>' +
																	'</li>';

																	break;

																case 'select' :

																	if ( element.dynamic_options == '0' ) {
																		html += '<li ' + ( element.hidden == 1 || element.hidden == "true" ? 'style="display: none;"' : '' )  + '>' +
																			'<div class="item-content">' +
																				'<div class="item-inner">' +
																					'<div class="item-title label">' +
																						element.label +
																					'</div>' +
																					'<div class="item-input item-input-field">' +
																						'<select id="' + element.id + '" ' + ( element.requred == '1' || element.required == 'true' ? ' required ' : '' ) + '>';

																							$$.each( JSON.parse( element.options ), function( index, option ) {
																								html += '<option value="' + option + '">' +
																									option +
																								'</option>';
																							} );

																						html += '</select>' +
																					'</div>' +
																				'</div>' +
																			'</div>' +
																		'</li>';
																	}

																	break;

																case 'multiple-select' :

																	if ( element.dynamic_options == '0' ) {
																		html += '<li ' + ( element.hidden == 1 || element.hidden == "true" ? 'style="display: none;"' : '' )  + '>' +
																			'<a href="#" class="item-link smart-select">' +
																				'<select multiple id="' + element.id + '" ' + ( element.requred == '1' || element.required == 'true' ? ' required ' : '' ) + '>';

																					$$.each( JSON.parse( element.options ), function( index, option ) {
																						html += '<option value="' + option + '">' +
																							option +
																						'</option>';
																					} );

																				html += '</select>' +
																				'<div class="item-content">' +
																					'<div class="item-inner">' +
																						'<div class="item-title">' + element.label + '</div>' +
																					'</div>' +
																				'</div>' +
																			'</a>' +
																		'</li>';
																	}

																	break;

																case 'date' :

																	var value = '';

																	if ( element.use_current_date == 1 || element.use_current_date == 'true' ) {
																		var date = new Date(),
																			month = ( date.getMonth() + 1 ),
																			day = date.getDate();

																		value = date.getFullYear() + '-' + ( month.toString().length == 1 ? '0' + month : month ) + '-' + ( day.toString().length == 1 ? '0' + day : day );
																	}

																	html += '<li ' + ( element.hidden == 1 || element.hidden == 'true' ? 'style="display: none;"' : '' )  + '>' +
																		'<div class="item-content">' +
																			'<div class="item-inner">' +
									                                            '<div class="item-title label">' +
									                                                element.label +
									                                            '</div>' +
									                                            '<div class="item-input item-input-field">' +
									                                                '<input type="date" ' + ( element.requred == '1' || element.required == 'true' ? ' required ' : '' ) + ' id="' + element.id + '" placeholder="' + element.name + '" ' + ( value.length != 0 ? ' value="' + value + '" readonly ' : '' ) + ' autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">' +
									                                            '</div>' +
									                                        '</div>' +
																		'</div>' +
																	'</li>';

																	break;

																case 'time' :

																	var value = '';

																	if ( element.use_current_time == 1 || element.use_current_time == 'true' ) {
																		var date = new Date();

																		value = '' + ( date.getHours().toString().length == 1 ? '0' + date.getHours() : date.getHours() ) + ':' + ( date.getMinutes().toString().length == 1 ? '0' + date.getMinutes() : date.getMinutes() ) + ':' + ( date.getSeconds().toString().length == 1 ? '0' + date.getSeconds() : date.getSeconds() );
																	}

																	html += '<li ' + ( element.hidden == 1 || element.hidden == "true" ? 'style="display: none;"' : '' )  + '>' +
																		'<div class="item-content">' +
																			'<div class="item-inner">' +
									                                            '<div class="item-title label">' +
									                                                element.label +
									                                            '</div>' +
									                                            '<div class="item-input item-input-field">' +
									                                                '<input type="time" id="' + element.id + '" ' + ( element.requred == '1' || element.required == 'true' ? ' required ' : '' ) + ' placeholder="' + element.name + '" ' + ( value.length != 0 ? ' value="' + value + '" readonly ' : '' ) + ' autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">' +
									                                            '</div>' +
									                                        '</div>' +
																		'</div>' +
																	'</li>';

																	break;

																case 'geo-point' :

																	html += '<li class="gps-field">' +
																		'<div class="item-content">' +
																			'<div class="item-inner">' +
																				'<div class="item-title label">' +
																					element.label +
																				'</div>' +
																				'<div class="item-input item-input-field">' +
																					'<input type="text" id="' + element.id + '" ' + ( element.requred == '1' || element.required == 'true' ? ' required ' : '' ) + ' readonly placeholder="' + element.name + '">' +
																					'<a href="javascript:void(0);" class="button gps-button" data-element-id="' + element.id + '" data-accuracy-required="' + element.accuracy + '">' +
																						'<i class="f7-icons">navigation_fill</i>' +
																					'</a>' +
																				'</div>' +
																			'</div>' +
																		'</div>' +
																	'</li>';

																	break;

																case 'photo' :

																	html += '<li class="photo-field" id="' + element.id + '">' +
																		'<div class="item-content">' +
																			'<div class="item-inner">' +
																				'<div class="item-title label">' +
																					element.label +
																				'</div>' +
																				'<div class="item-input">' +
																					'<div class="photo-slider">' +
																						'<div class="swiper-container swiper-' + element.id + '">' +
																							'<div class="swiper-pagination"></div>' +
																							'<div class="swiper-wrapper" data-image-container="' + element.id + '">' +
																							'</div>' +
																						'</div>' +
																					'</div>' +
																					'<a href="javascript:void(0);" data-min-images="' + element.min + '" data-max-images="' + element.max + '" data-format="' + element.file_ext + '" data-width="' + element.width + '" data-height="' + element.height + '" data-source="' + element.source + '" data-images-container-id="' + element.id + '"  class="button photo-button">' +
																						element.name +
																					'</a>' +
																				'</div>' +
																			'</div>' +
																		'</div>' +
																	'</li>';

																break;
															}

															if ( index == ( ordered_elements.length - 1 ) ) {
																section_index++;

																var button_html = '';

																if ( section_index == sections.length )
																	button_html = '<li class="item-content">' +
																		'<div class="item-inner">' +
																			'<a id="save_form" href="javascript:void(0);" class="button form-button button-fill">' +
																				'Save information' +
																			'</a>' +
																		'</div>' +
																	'</li>';

																page_html += html +
																			button_html +
																		'</ul>' +
																	'</div>' +
																'</div>';

																section_actively_rendering = false;

																if ( section_index == sections.length ) {

																	clearInterval( rendering_section_interval );

																	page_html += '</div>' +
																		'</div>' +
																	'</div>';

																	main_view.router.load({
																		content: page_html,
																		animatePages: false
																	} );

																	$$( '#save_form' ).on( 'click', function() {
																		var submissions = [];

																		$$.each( $$( '#form-tabs-content input:not([type="checkbox"]), #form-tabs-content textarea, #form-tabs-content select:not([multiple])' ), function( index, element ) {
																			submissions.push( {
																				element: parseInt( $$( element ).attr( 'id' ) ),
																			 	response: $$( element ).val()
																			} );
																		} );

																		$$.each( $$( '#form-tabs-content .label-switch input' ), function( index, element ) {
																			submissions.push( {
																				element: parseInt( $$( element ).attr( 'id' ) ),
																			 	response: ( $$( element ).is( ':checked' ) ? $$( element ).val() : $$( element ).attr( 'data-off-value' ) )
																			} );
																		} );

																		$$.each( $$( '#form-tabs-content .checkbox-list' ), function( index, container ) {
																			var selected = [],
																				total = $$( container ).find( 'input:checked' ).length;

																			$$.each( $$( container ).find( 'input:checked' ), function( count, element ) {
																				selected.push( $$( element ).val() );

																				if ( count == ( total - 1 ) )
																					submissions.push( {
																						element: parseInt( $$( container ).attr( 'id' ) ),
																						response: JSON.stringify( selected )
																					} );
																			} );
																		} );

																		$$.each( $$( '#form-tabs-content select[multiple]' ), function( index, container ) {
																			var selected = $$( container ).parent().find( '.item-after' ).text().split( ', ' );

																			submissions.push( {
																				element: parseInt( $$( container ).attr( 'id' ) ),
																				response: JSON.stringify( selected )
																			} );
																		} );

																		$$.each( $$( '#form-tabs-content .photo-field' ), function( index, container ) {
																			var photos = [],
																				total = $$( container ).find( 'img' ).length;

																			$$.each( $$( container ).find( 'img' ), function( count, element ) {
																				photos.push( $$( element ).attr( 'src' ) );

																				if ( count == ( total - 1 ) )
																					submissions.push( {
																						element: parseInt( $$( container ).attr( 'id' ) ),
																						response: JSON.stringify( photos )
																					} );
																			} );
																		} );

																		var date,
																			name = 'TEMP/SUB/' + random_string( 10 ) ;

																		db.transaction( function( tx ) {
																			tx.executeSql( 'INSERT INTO posts ( post_type_id, parent_id, name ) VALUES ( ( SELECT id FROM post_types WHERE singular = ? LIMIT 1 ), ?, ? )', [ 'Form submission', current_form_id, name ], function( tx, result ) {
																				var submission = result.insertId;

																				tx.executeSql( 'SELECT id FROM post_types WHERE singular = ? LIMIT 1', [ 'Form element response' ], function( tx, result ) {
																					if ( result.rows.length == 1 ) {
																						var post_type_id = result.rows.item( 0 ).id;

																						$$.each( submissions, function( index, value ) {
																							var name = 'TEMP/RES/' + random_string( 10 );

																							update_post( post_type_id, name, value, null, submission );

																							var date = new Date(),
																								month = ( date.getMonth() + 1 ),
																								day = date.getDate();

																							var meta_value = date.getFullYear() + '-' + ( month.toString().length == 1 ? '0' + month : month ) + '-' + ( day.toString().length == 1 ? '0' + day : day ) +
																								' ' + ( date.getHours().toString().length == 1 ? '0' + date.getHours() : date.getHours() ) + ':' + ( date.getMinutes().toString().length == 1 ? '0' + date.getMinutes() : date.getMinutes() ) + ':' + ( date.getSeconds().toString().length == 1 ? '0' + date.getSeconds() : date.getSeconds() );

																							update_post_meta( submission, 'timestamp', meta_value );

																							if ( index == ( submissions.length - 1 ) ) {
																								main_view.router.back( {} );

																								app.addNotification( {
																									message: 'Form responses successfully recorded.',
																									hold: 3000,
																									button: {
																										text: 'OK',
																										color: 'white',
																										close: true
																									}
																								} );
																							}
																						} );
																					}
																				} )
																			} );
																		} );
																	} );

																	$$( '#form-tabs-content .tab' ).on( 'tab:show', function() {
																		var index = $$( this ).index(),
																			total = ( $$( this ).siblings().length + 1 ),
																			hide = $$( this ).attr( 'data-hide-tab' ),
																			id = $$( this ).attr( 'id' );

																		setTimeout( function() {
																			if ( hide == 'true' ) {
																				if ( index == 0 && total > 1 )
																					app.showTab( '#' + $$( '#' + id ).parent( 'div' ).children( 'div:nth-child(2)' ).attr( 'id' ) );

																				else if ( index == ( total.length - 1 ) )
																					app.showTab( '#' + $$( '#' + id ).parent( 'div' ).children( 'div:nth-child(' + index + ')' ).attr( 'id' ) );

																				else
																					app.showTab( '#' + $$( '#' + id ).parent( 'div' ).children( 'div:nth-child(' + ( index + 1 ) + ')' ).attr( 'id' ) );
																			}
																		}, 10 );
																	} );

																	$$( '.photo-button' ).on( 'click', function() {
																		var button = $$( this ),
																			options = {
																				quality: 75,
																				destinationType: Camera.DestinationType.DATA_URL,
																				sourceType: button.attr( 'data-source' ) == 'camera' ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY,
																				encodingType: button.attr( 'data-format' ) == 'jpg' ? Camera.EncodingType.JPEG : Camera.EncodingType.PNG,
																				mediaType: Camera.MediaType.PICTURE,
																				cameraDirection: Camera.Direction.BACK
																			};

																		if ( !( button.attr( 'data-width' ) == '' && button.attr( 'data-height' ) == '' ) ) {
																			var dimensions = {};

																			if ( button.attr( 'data-width' ) != '' )
																				dimensions = Object.assign( {}, dimensions, {
																					targetWidth: parseInt( button.attr( 'data-width' ) )
																				} );

																			if ( button.attr( 'data-height' ) != '' )
																				dimensions = Object.assign( {}, dimensions, {
																					targetHeight: parseInt( button.attr( 'data-height' ) )
																				} );

																			options = Object.assign( {}, options, dimensions, {
																				allowEdit: true,
																			} );
																		}

																		navigator.camera.getPicture( function( imageData ) {
																			var container = $$( '[data-image-container="' + button.attr( 'data-images-container-id' ) + '"]' );

																			if ( ( container.find( 'img' ).length + 1 ) == parseInt( button.attr( 'data-max-images' ) ) )
																				button.hide();

																			var html = '<div class="swiper-slide">' +
																				'<img src="data:image/' + ( button.attr( 'data-format' ) == 'jpg' ? 'jpeg' : 'png' ) + ';base64,' + imageData + '">' +
																			'</div>';

																			container.append( html );

																			var slidesPerView  = 1;

																			if ( $$( window ).width() <= 320 )
																				slidesPerView = 1;

																			else if ( $$( window ).width() <= 480 )
																				slidesPerView = 2;

																			else if ( $$( window ).width() <= 720 )
																				slidesPerView = 3;

																			else
																				slidesPerView = 4;

																			if ( slidesPerView > parseInt( button.attr( 'data-max-images' ) ) )
																				slidesPerView = parseInt( button.attr( 'data-max-images' ) );

																			var swiper = app.swiper( '.swiper-' + button.attr( 'data-images-container-id' ), {
																				pagination:'.swiper-' + button.attr( 'data-images-container-id' ) + ' .swiper-pagination',
																				spaceBetween: 10,
																				slidesPerView: slidesPerView
																			} );

																			var imageWidth = $$( '.swiper-' + button.attr( 'data-images-container-id' ) + ' .swiper-slide:first-child' ).width();

																			$$( '.swiper-' + button.attr( 'data-images-container-id' ) + ' .swiper-slide img' ).css( {
																				'max-width': imageWidth + 'px'
																			} );
																		}, null, options );
																	} );

																	$$( '.gps-button' ).on( 'click', function() {
																		var button = $$( this );

																		if ( !button.hasClass( 'reading' ) ) {
																			button.addClass( 'reading' );

																			app.showPreloader();

																			navigator.geolocation.getCurrentPosition( function( position ) {
																				var number = button.attr( 'data-element-id' ),
																					requiredAccuracy = button.attr( 'data-accuracy-required' ),
																					cordinates = position.coords;

																				if ( ( cordinates.accuracy / 100 ) >= parseFloat( requiredAccuracy ) )
																					$$( '#' + number ).val(
																						cordinates.latitude.toFixed( 7 ) + ', ' + cordinates.longitude.toFixed( 7 ) +
																						( parseInt( cordinates.altitude ) > 0 ? ', ' + cordinates.altitude.toFixed( 7 ) : '' )
																					).trigger( 'blur' );

																				button.removeClass( 'reading' );

																				app.hidePreloader();
																			}, function() {
																				button.removeClass( 'reading' );

																				app.hidePreloader();
																			},
																			{
																				maximumAge: 3000,
																				timeout: 5000,
																				enableHighAccuracy: true
																			} );
																		}
																	} );

																	app.hidePreloader();
																}
															}
														} );
													}
												}, 50 );
											}
										} );
									} );
								} );
							} );
						}
					} );
				}
			} );
		} );
	} );
}

function start_automatic_synchronization() {
	var actively_synchronizing = false;

	var synchronization_interval = setInterval( function() {
		if ( actively_synchronizing == false && currentUser.remote_id != null ) {
			actively_synchronizing = true;

			var actively_synchronizing_submission =  false;

			var synchronization_submission_interval = setInterval( function() {
				if ( actively_synchronizing_submission == false ) {
					db.transaction( function( tx ) {
						tx.executeSql( 'SELECT post_meta.meta_value AS timestamp, posts.parent_id AS form, posts.id AS id FROM posts LEFT JOIN post_meta ON posts.id = post_meta.post_id WHERE post_meta.meta_key = ? AND posts.post_type_id = ( SELECT id FROM post_types WHERE singular = ? LIMIT 1 ) AND posts.remote_id IS NULL LIMIT 1', [ 'timestamp', 'Form submission' ], function( tx, result ) {
							if ( result.rows.length == 1 ) {
								var timestamp = result.rows.item( 0 ).timestamp,
									submission = result.rows.item( 0 ).id,
									form = result.rows.item( 0 ).form;

								tx.executeSql( 'SELECT remote_id FROM posts WHERE id = ? LIMIT 1', [ form ], function( tx, result ) {
									if ( result.rows.length == 1 ) {
										var form = result.rows.item( 0 ).remote_id;

										$$.post(
											api_base + 'form/' + form + '/submissions/add',
											{
												user: currentUser.remote_id,
												timestamp: timestamp
											},
											function( response ) {
												response = JSON.parse( response );

												if ( response.result == 'successful' ) {
													db.transaction( function( tx ) {
														tx.executeSql( 'UPDATE posts SET remote_id = ?, name = ? WHERE id = ?', [ response.data.id, response.data.name, submission ], function( tx, result ) {
															var remote_submission_id =  response.data.id;

															var responses = [];

															tx.executeSql( 'SELECT id FROM posts WHERE parent_id = ? AND remote_id IS NULL', [ submission ], function( tx, result ) {
																if ( result.rows.length == 0 )
																	actively_synchronizing_submission = false;

																for ( i = 0; i < result.rows.length; i++ ) {
																	var record = result.rows.item( i );

																	responses[ record.id ] = {
																		id: record.id,
																		submission: submission,
																		element: null,
																		response: null
																	};
																}

																var count = 0;

																$$.each( responses, function( index, response ) {
																	if ( typeof responses[ index ] != 'undefined' )
																		db.transaction( function( tx ) {
																			tx.executeSql( 'SELECT meta_key, meta_value FROM post_meta WHERE post_id = ?', [ response.id ], function( tx, result ) {
																				for ( i = 0; i < result.rows.length; i++ ) {
																					var record = result.rows.item( i );

																					if ( record.meta_key == 'element' ) {
																						tx.executeSql( 'SELECT remote_id FROM posts WHERE id = ? LIMIT 1', [ record.meta_value ], function( tx, result ) {
																							if ( result.rows.length == 1 )
																								responses[ index ].element = result.rows.item( 0 ).remote_id;
																						} );
																					}

																					else if ( record.meta_key == 'response' )
																						responses[ index ].response = record.meta_value;
																				}

																				setTimeout( function() {
																					$$.post(
																						api_base + 'form-submission/' + remote_submission_id + '/responses/add',
																						{
																							user: currentUser.remote_id,
																							response: responses[ index ].response,
																							form_element: responses[ index ].element
																						},
																						function( response ) {
																							ajax_response = JSON.parse( response );

																							if ( response.result == 'successful' ) {
																								db.transaction( function( tx ) {
																									tx.executeSql( 'UPDATE posts SET remote_id = ?, name = ? WHERE id = ?', [ response.data.id, response.data.name, responses[ index ].id ] );
																								} );
																							}
																						}
																					);
																				}, 1000 );

																				count++;

																				if ( count == responses.length )
																					actively_synchronizing_submission = false;
																			} );
																		} );
																} );
															} );
														} );
													} );
												}
											},
											function() {
												actively_synchronizing_submission = false;
											}
										);
									}

									else
										actively_synchronizing_submission = false;
								} );
							}

							else
								actively_synchronizing_submission = false;
						} );
					} );
				}
			}, 3000 );
		}
	}, 1000 );
}
